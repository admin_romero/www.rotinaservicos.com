<?php 
$title		= 'Teste';
$desc       = 'Rotina Serviços Terceirizados - fundada em 1997 e desde então atua nas áreas de portaria e controle de acesso, limpeza, zeladoria e conservação de condomínios residenciais, comércios e empresas em geral.';
$key        = 'terceirizados, servicos, servicos prediais, predios, ap, casa, condominio';
$var        = 'Inicio';
include 'inc/head.php';
include 'inc/topo.php';
include 'inc/lightbox.php';
?>
</head>
<!--Slider-->
<div class="clearbox">
	<?php include 'slider.php';?>
</div>
<!--CONTEÚDO-->
	<section id="block-content">
		<div class="content-servicos">
			<div class="items items-anime">
				<div class="container">
					<div class="picture-item" style="background-image: url('<?=$url?>img/portaria.jpg');"></div>
					<div class="text-item">
						<div style="grid-row: 1;">
							<h2>Portaria / Controle de acesso</h2><br>
								<p>É como o cartão de visitas do local, devendo sempre manter um 	clima cordial, respeitoso e profissional em seu posto de trabalho.
								Este profissional tem como suas principais atividades:<br><br>
								<span></span>
								a) receber e distribuir correspondências destinadas aos condôminos;<br>
								b) transmitir e cumprir ordens do zelador ou síndico;<br>
								c) fiscalizar a entrada de pessoas e veículos;<br>
								d) dar conhecimento ao zelador ou síndico de todas as reclamações e sugestões que ocorrerem durante seu turno.
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="items items-anime" style="background-color: #fff">
				<div class="container">
					<div class="clearfixpic">
						<div class="picture-item" style="grid-column: 2;background-image: url('<?=$url?>img/limpeza.jpg');background-position-x: -100px"></div>
					</div>
					<div class="text-item" style="grid-column: 1/2;">
						<div style="grid-row: 1;">
							<h2>Limpeza</h2><br>
							<p>O profissional designado auxiliar de limpeza é aquele que se dedica profissionalmente ao asseio de locais como condomínios residenciais, empresas, indústrias, etc. Tem como principais atividades a conservação e limpeza de edifícios e outros locais, além de efetuar a remoção de lixo, fazer lavagem de vidraças e persianas, desentupir ralos e pias e executar outras tarefas da mesma natureza. Nossos colaboradores são instruídos a sempre fazer uso de EPIs (equipamento de proteção individual) conforme cada necessidade, além de atentar sempre para o correto uso de produtos de limpeza e equipamentos. Devido à especificidade de cada posto de trabalho, nossa equipe de supervisão montará uma rotina de serviços junto à cada condomínio ou empresa, buscando assim atender integralmente às necessidades e preferências de cada um de nossos clientes.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="items items-anime">
				<div class="container">
					<div class="picture-item" style="background-image: url('<?=$url?>img/zelador.jpg');"></div>
					<div class="text-item">
						<div style="grid-row: 1;">
							<h2>Zeladoria</h2><br>
							<p>É o funcionário que trabalha na área de limpeza e manutenção completa dos edifícios. <br><br>
							O Zelador é o profissional a quem compete:<br>
							<span></span>
							a) ter contato direto com a administração do condomínio, agindo como preposto do síndico;<br>
							b) transmitir as ordens dos superiores e fiscalizar seu cumprimento; <br>
							c) fiscalizar as áreas de uso comum, verificar o funcionamento das instalações hidráulicas e elétricas, assim como aparelhos de uso comum, além de zelar pela observância da disciplina no edifício, de acordo com seu regimento interno. Oferecemos também a modalidade de contratação de apenas o posto de trabalho de zelador, em condições especiais, caso seu condomínio já disponha de terceirização de outros postos, a fim de dinamizar os trabalhos e supervisionar serviços prestados por outras empresas.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="items items-anime" style="background-color: #fff">
				<div class="container">
					<div class="clearfixpic">
						<div class="picture-item" style="grid-column: 2;background-image: url('<?=$url?>img/jardinagem.jpg');"></div>
					</div>
					<div class="text-item" style="grid-column: 1/2">
						<div style="grid-row: 1">
							<h2>Jardinagem</h2><br>
							<p>A manutenção e conservação de áreas ajardinadas é uma das atividades mais corriqueiras em condomínios residenciais e empresas. São locais que geram sensação de conforto e bem-estar aos seus frequentadores, e devem ter um cronograma bem definido de cuidados. Nossa equipe de jardinagem é treinada e capacitada para a realização destes serviços, que incluem: poda de diferentes espécies arbóreas, limpeza, controle de pragas e doenças, adubação e fertilização do solo.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="items items-anime">
				<div class="container">
					<div class="picture-item" style="background-image: url('<?=$url?>img/predial.jpg');"></div>
					<div class="text-item">
						<div style="grid-row: 1">
							<h2>Manutenção Predial</h2><br>
							<p>O profissional denominado auxiliar de manutenção deve estar preparado para detectar problemas, fazer o diagnóstico e solucioná-los. Quando necessário, deve solicitar orçamentos e acompanhar o reparo. Desta forma, é de fundamental importância que sejam realizadas periódicas vistorias, definidas quando da elaboração da rotina de serviços junto à administração do condomínio. Ressaltamos que nossos colaboradores são selecionados e treinados para lidarem bem com moradores e visitantes, de forma a buscar tal interação sempre que necessário, unindo sólida experiência e conhecimento técnico.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--Pagina empresa-->
	<section id="block-empresa">
		<div class="content-empresa">
			<div class="container">
				<div class="titles-box items-anime" style="opacity: 0;">
					<div class="titles-box-items">
						<h1>A Empresa</h1>
						<h2 style="font-size: 20px;font-weight: normal;color: #555;margin-top: 10px;">Rotina Serviços Terceirizados</h2>
						<br><br>
						<p style="padding: 5px;">A ROTINA SERVIÇOS foi fundada em 1997 e desde então atua nas áreas de portaria e controle de acesso, limpeza, zeladoria e conservação de condomínios residenciais, comércios e empresas em geral.<br><br>Prezamos pela excelência e total atendimento às necessidades de nossos clientes, buscando completa satisfação, 24 horas ao dia, sete dias na semana. Enfatizamos que, entre nossas maiores premissas, estão o atendimento a qualquer momento do dia, através de nossa supervisão, como também a criteriosa seleção de nossos colaboradores, garantindo sempre segurança e eficiência em nossos serviços, além de também garantir preços extremamente competitivos e justos.</p>
					</div>
				</div>
				<div class="about-items items-anime" style="opacity: 0;">
					<div style="width: 75%;">
						<div class="grid-items animated fadeInLeft" style="grid-column: 1">
							<h1>
								<font style="color: #0064a8; margin-right: 5px;"><i class="fas fa-bullseye"></i></font>
							Visão
					</h1>
							<p>A excelência na prestação de serviços, funcionários capacitados e comprometidos e o melhor custo-benefício do segmento são os diferenciais para que possamos sempre manter uma parceria sadia e próspera, cumprindo a missão da empresa.</p>
						</div>
						<div class="grid-items animated fadeInRight" style="grid-column: 2">
							<h1>
								<font style="color: #0064a8; margin-right: 5px;"><i class="far fa-check-circle"></i></font>
								Missão
							</h1>
							<p>Alcançar a melhor parceria com nossos clientes através do pronto atendimento de suas necessidades, a qualquer momento do dia, de modo a criar um elo sempre sólido, consistente e rotineiro. A satisfação completa de nossos clientes será sempre um incessante alvo em todos os momentos de nossa atuação.</p>
						</div>
						<div class="grid-items animated fadeInLeft" style="grid-column: 3">
							<h1>
								<font style="color: #0064a8; margin-right: 5px;"><i class="far fa-handshake"></i></font>
								Valores
							</h1>
							<p>Disseminar o conceito da SUSTENTABILIDADE; Investir na CAPACITAÇÃO constante dos nossos colaboradores; Ter sempre o mais aprimorado RELACIONAMENTO com nossos clientes; Buscar o melhor CUSTO-BENEFÍCIO para seu condomínio ou empresa; RESPEITO por você.</p>
						</div>
						<div class="grid-items animated fadeInRight" style="grid-column: 4;padding-bottom: 30px;" id="grid-exp">
							<h1>
								<font style="color: #0064a8; margin-right: 5px;"><i class="far fa-thumbs-up"></i></font>
								Vantagens
							</h1>
							<p id="paragrafo-exp" style="height: auto;">Profissionais experientes e qualificados em cada ramo de atuação; Pronta reposição em caso de necessidade; Encargos sociais e trabalhistas, assim como impostos incidentes para emissão de notas fiscais, a cargo da prestadora; Prevenção contra processos trabalhistas.<br><br>Caso seu condomínio já possua um quadro de funcionários ou, ainda, possua qualquer posto que seja desejada sua  manutenção por parte do condomínio, tal fato não será um impeditivo para a adoção da terceirização, já que a ROTINA SERVIÇOS se comprometerá, conforme cada caso, a agregar este funcionário ao seu quadro de colaboradores, sempre buscando a satisfação total do cliente.<br><br>Portanto, através desta breve apresentação, esperamos que se torne mais fácil a compreensão de que, conforme nosso lema diz, TERCEIRIZAR É ECONOMIZAR.<br><br>Terceirize em seu condomínio e vivencie também todas essas vantagens!</p>
							<input type="checkbox" class="blue">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
<?php include('inc/footer.php')?>
<div class="lightbox">
	<a href="javascript:;" id="closelbox" title="Fechar">&times;</a>
	<div class="wrapper">
		<img src="<?=$url?>img/comunicado.jpg" alt="Comunicado COVID-19" title="Comunicado COVID-19" width="1000">
	</div>
	<div class="bdark"></div>
</div>
</body>
<!--carregamento da página-->
<script defer="defer">
$(document).ready(function(){
	//popup
	lightbox();
	//esconder loading
	$('div#loading').fadeToggle(500);
	//efeitos
	setTimeout(function(){
		$('div.slider').addClass('animated fadeIn');
		$('div.picture-item').addClass('animated fadeIn');
		setTimeout(function(){
			$('blockquote.blockquote-items').addClass('animated fadeInDown');
			setTimeout(function(){
				$('button.button-slider').addClass('animated fadeInUp');
				setTimeout(function(){
					$('img#logoslider').addClass('animated fadeInLeft');
				},850)
			},800);
		},500);
	},150);
});
</script>
<!--script do site completo-->
<script defer="defer" src="js/script.js"></script>
</html>