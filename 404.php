<?php 
//escondendo os erros
$page404=true;
error_reporting(0);
ini_set(“display_errors”, 0 );
include 'inc/head.php';
include 'inc/topo.php';
?>
	<!--Slider-->
	<div class="clearbox">
		<?php include 'slider.php'; ?>
	</div>
	<div id="notfoundpage">
		<div class="container">
			<div class="notfound">
				<div class="animated fadeInUp">
					<h2>404: Página não encontrada</h2>
					<br>
					<p>Oops! Parece que esta página não existe no site. <a href="<?=$url?>">Clique para voltar para o site.</a></p>
					<br>
					<h3>O que pode ser?</h3>
					<ul class="ul-list">
						<li>Verifique se você digitou o endereço correto.</li>
						<li>Verifique se a sua conexão com a internet é estável.</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
<?php include 'inc/footer.php';?>
</body>
<!--carregamento da página-->
<script defer="defer">
	$(document).ready(function(){
		//esconder loading
		$('div#loading').fadeToggle(500);
		//efeitos
		setTimeout(function(){
			$('div.slider').addClass('animated fadeIn');
			$('div.picture-item').addClass('animated fadeIn');
			setTimeout(function(){
				$('blockquote.blockquote-items').addClass('animated fadeInDown');
				setTimeout(function(){
					$('button.button-slider').addClass('animated fadeInUp');
					setTimeout(function(){
						$('img#logoslider').addClass('animated fadeInLeft');
					},850)
				},800);
			},500);
		},150);
	});
</script>
<!--script do site completo-->
<script src="js/script.js"></script>
</html>