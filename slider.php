<?php 
include 'geral.php';
?>
<section id="slider-box">
	<div class="slider" style="opacity: 0;">
		<div class="slider-text" style="background-image: url('<?=$url?>img/slide01.jpg');">
			<ul id="text-info-box">
				<li style="width: 600px;display: flex;align-items: center;justify-content: center;">
					<a href="<?=$url?>"style="width: 20%; float: left;margin-right: 10px;" id="logoslider-link"><img src="<?=$url?>img/logo2.png" id="logoslider" style="opacity: 0" ></a>
					<blockquote style="background-color: transparent;padding-left: 20px;" class="blockquote-items">
						<h1><?=$nomeSite?></h1>
						<p style="margin-top: 5px;color: #444"> <?=$sloganSite?></p>
						<a href="#block-empresa" class="goto"><button class="button-slider" style="margin-top: 10px;font-weight: 400;">Saiba mais</button></a>
					</blockquote>
				</li>
			</ul>
		</div>
		<div style="position: absolute;top:0;width: 100%;height: 100%;background-color: rgba(255,255,255,.5);"></div>
	</div>
	<div id="loading" style="width: 100%;height: 100%;display: flex;justify-content: center;align-items: center;padding-top: 70px;z-index: 1;">
		<span style="text-align: center;">
			<img src="<?=$url?>img/loading.gif" style="width: 5%">
		</span>
	</div>
</section>