<form class="formSendContact" style="grid-column: 1;">
		<div style="z-index: 10;">
			<h1>Formulário para contato</h1>
			<p>Envie seu contato preenchendo o formulário abaixo.</p>
			<textarea class="item" name="msg" placeholder="Mensagem..."><?php
              if (isset($_POST['msg'])): echo $_POST['msg'];
              endif;
              ?></textarea>
			<!-- <input type="text" name="assunto" placeholder="Assunto" required> -->
			<select class="item" name="assunto">
				<option value="default" selected>Selecione o motivo de contato*</option>
				<option value="Cliente/Orçamento">Cliente/Orçamento</option>
				<option value="Colaborador/Currículo">Colaborador/Currículo</option>
			</select>
			<input class="item" type="text" value="<?php
              if (isset($_POST['nome'])): echo $_POST['nome'];
              endif;
              ?>" name="nome" placeholder="Nome completo*">
			<input type="text" class="item" value="<?php
              if (isset($_POST['email'])): echo $_POST['email'];
              endif;
              ?>" name="email" placeholder="E-mail*">
			<div class="file">
				<span><i class="fas fa-paperclip"></i> Anexar um arquivo</span>
				<input class="item" type="file" name="uploaded_file">
			</div>
			<script>
				$('input[type="file"]').change(function() {
				    $(this).prev().html($(this).val());
				});
			</script>
			<br>
			<div class="g-recaptcha" style="transform:scale(0.8);-webkit-transform:scale(0.8);transform-origin:0 0;-webkit-transform-origin:0 0;float: left;" data-sitekey="<?= $Key; ?>"></div>
			<span style="font-size: 12px; float: right;">* Itens obrigatórios</span><br>
		</div>
		<span id="spanmsg"></span>
		<div class="formspan">
			<span></span>
		</div>
		<div class="formloading"></div>
	</div>
	</div>
	<div style="width: 100%;display: flex;justify-content: center;"><div id="pop"></div></div>
	<div style="width: 100%;display: flex;justify-content: center;margin-top: 5px;"><button type="submit" name="envform" class="sendform">Enviar contato</button></div>
</form>