<footer id="block-footer">
	<div class="box-footer items-anime" style="opacity: 0;">
		<div class="head-footer">
			<div class="container" style="float: left;">
				<div class="grid-footer">
					<?php 
						if(!$page404){
					?>
					<div>
						<div style="margin-bottom: 80px;">
							<div class="formbox" style="grid-template-columns: repeat(2,1fr);grid-gap: 40px;">
								<div style="grid-column: 2;">
									<h1>Vamos conversar pessoalmente</h1>
									<p>Entre em contato diretamente conosco!</p>
									<br>
									<iframe src="<?=$mapaIframe?>" width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
								</div>
								<div style="grid-row: 1;">
								<?php
								include('contato.php');
								?>
						</div>
					</div>
					<?php }?>
					<div style="float: left;grid-column: 1;margin-bottom: 40px;grid-template-columns: repeat(2,1fr); grid-gap: 40px;" id="items-footer">
						<div style="grid-column: 1/3;grid-template-columns: repeat(2,1fr);grid-gap: 40px" id="items-footer-items">
							<div style="grid-column: 1">
								<h2>Contato</h2><br>
								<p>
									<font style="font-size: 24px;color: #444"><a href="tel:<?=$fone[1]?>"><i class="fas fa-phone-alt"></i> <?=$ddd.' '.$fone[1]?></font></a><br><br>
									<a href="mailto:<?=$contatoEmail?>"><i class="fas fa-envelope"></i> &nbsp;<?=$contatoEmail;?></a> <br>
									<i class="fas fa-map-marker-alt"></i> &nbsp; <?=$contatoAddress.', Cep '.$contatoCep.', '.$UF;?>
								</p>
							</div>
							<div style="grid-column: 2;text-align: left;">
								<div style="grid-column: 3;display: grid;grid-template-columns: repeat(2,1fr);tex">
									<div style="grid-column: 1">
										<h2>Navegar pelo site</h2><br>
										<p>
											<ul class="footer-menu">
												<li><a href="#fixtopo" class="goto">Topo</a></li>
												<li><a href="#block-content" class="goto">Serviços</a></li>
												<li><a href="#block-empresa" class="goto">A Empresa</a></li>
												<li><a href="#block-footer" class="goto">Contato</a></li>
											</ul>	
										</p>
									</div>
									<div style="grid-column: 2">
										<h2>Últimos artigos</h2><br>
										<p>
											<ul class="footer-menu">
												<li><a target="_blank" rel="nofollow" href="https://rotinaservicos.tumblr.com/post/151765904557/obras-em-condominios">Obras em condomínios</a></li>
												<li><a target="_blank" rel="nofollow" href="https://rotinaservicos.tumblr.com/post/150122406197/seguro-condominial">Seguro condominal</a></li>
												<li><a target="_blank" rel="nofollow" href="https://rotinaservicos.tumblr.com/post/149326863687/fracoes-ideais-condominios">Frações ideais em condomínios</a></li>
												<li><a target="_blank" rel="nofollow" href="https://rotinaservicos.tumblr.com/post/148810893322/o-que-e-area-comum-area-privativa">O que é área comum e área privativa?</a></li>
											</ul>	
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="feet-footer">
			<div>
				<div class="stick-green"></div>
				<div class="stick-blue"></div>
			</div>
			<div class="boxfeet">
				<span>© 2020 Romero Service LTDA. Todos os direitos reservados.</span>
				<span><?php include 'canais.php';?></span>
			</div>
		</div>
	</div>
</footer>
<script src="<?=$url?>js/sendform.js"></script>	