<?php
//escondendo os erros
error_reporting(0);
ini_set(“display_errors”, 0 );
//variaveis do site
include('inc/geral.php')
?>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
<link rel="icon" href="img/logo2.png" type="image/png/">
<link rel="apple-touch-icon" href="img/logo2.png" type="image/png"/>
<link href="css/fonts/open-sans.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/queries.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="fontawesome/css/all.css">
<link rel="stylesheet" type="text/css" href="css/animate.css">

<meta name="description" content="<?=ucfirst($desc)?>">
<meta name="keywords" content="<?=str_replace($prepos,', ', $h1).', '.$nomeSite?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="geo.position" content="<?=$latitude.";".$longitude?>">
<meta name="geo.placename" content="<?=$cidade."-".$UF?>">
<meta name="geo.region" content="<?=$UF?>-BR">
<meta name="ICBM" content="<?=$latitude.";".$longitude?>">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="<?=$url.$urlPagina?>">

<meta property="og:region" content="Brasil">
<meta property="og:title" content="<?=$title." - ".$nomeSite?>">
<meta property="og:type" content="article">
<meta property="og:url" content="<?=$url.$urlPagina?>">
<meta property="og:description" content="<?=$desc?>">
<meta property="og:site_name" content="<?=$nomeSite?>">
<script src="js/jquery-3.4.1.min.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<meta charset="utf-8">
<title><?=$title.' - '.$nomeSite?></title>