<?php 
include 'geral.php';
?>
<link rel="stylesheet" type="text/css" href="css/lightbox.css">
</head>
<body>
<!--TOPO-->
<div id="fixtopo" style="position: absolute;top: 0;width: 100%;opacity: 0;"></div>
<!--header nav-->
<header class="header-nav" id="header-topo" style="padding: 10px;">
<a id="logotop" href="<?=$url?>" style="display: flex; align-items: center;text-align: center; font-size: 14px; font-weight: normal;"><img src="img/logo2.png" alt="Logotipo" style="width: 15%;"> <font style="margin: 5px;font-weight: bold; color: #555">/rotinaservicos</font></a>
<nav id="nav-menu">
	<ul>
		<li class="nomobile"><a href="#fixtopo"class="goto selected" id="home-btn-menu">Topo</a></li>
		<li class="nomobile"><a href="#block-content"class="goto" id="servicos-btn-menu">Serviços</a></li>
		<li class="nomobile"><a href="#block-empresa"class="goto" id="empresa-btn-menu">A Empresa</a></li>
		<li class="nomobile"><a href="#block-footer"class="goto" id="contato-btn-menu">Contato</a></li>
		<li id="btn-mobile" style="cursor: pointer;padding: 10px 15px">
			<i class="fas fa-bars"></i>
			<div class="navdrop-mobile">
				<ul class="menu-mobile">
					<li class="menu-mobile">
						<a href="#fixtopo"class="goto selected" id="home-btn-menu">Topo</a></li>
					<li class="menu-mobile">
						<a href="#block-content"class="goto" id="servicos-btn-menu">Serviços</a></li>
					<li class="menu-mobile">
						<a href="#block-empresa"class="goto" id="empresa-btn-menu">A Empresa</a></li>
					<li class="menu-mobile">
						<a href="#block-footer"class="goto" id="contato-btn-menu">Contato</a></li>
				</ul>
			</div>
		</li>
	</ul>
	<script>
		//Versão para mobile
		//navbar
		var mobile=0;
		$('li#btn-mobile').click(function(e){
			e.preventDefault();
			mobile++;
			if(mobile>0 && mobile<2){
				$('.navdrop-mobile').show(250);
			} else {
				$('.navdrop-mobile').hide(250);
				mobile=0;
			}
		})
	</script>
</nav>
</header>