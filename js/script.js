//ANIME SCROLL AO ROLAR
(function() {
    debounce = function(func, wait, immediate) {
        var timeout;
        return function() {
            var context = this,
                args = arguments;
            var later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    }
    var $target = $('.items-anime'),
        offset = $(window).height()*5/4,
        valitem = 0;

    if($(window).width()>=765){
        offset=offset;
    }else{
        offset=offset*8/4;
    }
    function animeScroll() {
        $target.each(function() {
            var itemTop = $(this).offset().top;
            valitem++;
            if ($(document).scrollTop() > itemTop - offset) {
                $(this).css({'opacity':'1'});
                if($(window).width()>=765){
                    if(valitem%2 && valitem!=1){
                        $(this).addClass('animated slideInLeft');
                    } else {
                        $(this).addClass('animated slideInRight');
                    }
                }else {
                    $(this).addClass('animated fadeIn');
                }
            }
        });
    }
    animeScroll();
    $(document).scroll(debounce(function() {
        animeScroll();
    }, 50));
}());
$('a.goto').click(function(e) {
    e.preventDefault();
    var id = $(this).attr('href');
    targetId = $(id).offset().top;
    $(document).ready(function(){
        $('html, body').animate({
            scrollTop: targetId - 60
        }, 500);
    });
});
$(window).scroll(function() {
    if ($(window).scrollTop() > 70) {
        $("header.header-nav").addClass('menu-fixo');
        $('div.navdrop-mobile').css({
            'top': '60px'
        });
        $("a#logotop").addClass('animated flipInX');
    } else {
        $("header.header-nav").removeClass('menu-fixo');
        $('div.navdrop-mobile').css({
            'top': '70px'
        });
        $("a#logotop").removeClass('animated flipInX');
    }
    if ($(window).scrollTop() < $('#block-content').offset().top - 60) {
        $('li a').removeClass('selected'); //desativa classe
        $('li a#home-btn-menu').addClass('selected');
        $('li a#home-btn-mobile-menu').addClass('selected');
    } else {
        $('li a').removeClass('selected'); //desativa classe
        $('li a#servicos-btn-menu').addClass('selected');
        $('li a#servicos-btn-mobile-menu').addClass('selected');
    }

    if ($(window).scrollTop() > $('#block-empresa').offset().top - 80) {
        $('li a').removeClass('selected'); //desativa classe
        $('li a#empresa-btn-menu').addClass('selected');
        $('li a#servicos-btn-mobile-menu').addClass('selected');
    }
    if ($(window).scrollTop() > $('#block-footer').offset().top - 70) {
        $('li a').removeClass('selected'); //desativa classe
        $('li a#contato-btn-menu').addClass('selected');
        $('li a#servicos-btn-mobile-menu').addClass('selected');
    }
});
var exp = 0;
$('input[type=checkbox].blue').click(function() {
    exp++;
    var bodyScroll = $(document).scrollTop();
    if (exp == 1) {
        $('#grid-exp').css({
            'height': 'auto',
            'transition': 'all .3s ease-out'
        });
        $('#grid-exp').animate({
            'height': '+=30px'
        }, 0);
    } else {
        $('#grid-exp').css({
            'height': '285px'
        });
        exp = 0;
    }
});