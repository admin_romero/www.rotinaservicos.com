$(function(){
	var loading='<div class="formspan"><img src="./img/loading.gif" style="width: 50px;"></div>';
	function checkInput(){
		$('input.item').keyup(function(){
			if($(this).val()!=""){
				$(this).css({'background':'#fff','border':'none'});
			}
		});
		$('textarea.item').keyup(function(){
			if($(this).val()!=""){
				$(this).css({'background':'#fff','border':'none'});
			}
		});
		$('select.item').click(function(){
			if($(this).val()!='default'){
				$(this).css({'background':'#fff','border':'none'});
			}
		})
	}
	$('form.formSendContact').submit(function(e){
		e.preventDefault();
		//variaveis
		var email = $('input[name="email"]').val(),
			assunto = $('select[name="assunto"]').val(),
			msg_info=false,
			$env=false;
		//verificacoes dos campos
		var emailFilter=/^.+@.+\..{2,}$/,
		 	illegalChars= /[\(\)\<\>\,\;\:\\\/\"\[\]]/
		if(!(emailFilter.test(email))||email.match(illegalChars)){
			msg_info="<strong>Atenção: </strong>Informe um e-mail válido.";
			$('.item').css({'background':'#f2dede','border':'1px solid #dd4747'});
			checkInput();
			$('div.formspan').hide();
		} else {
			$('#spanmsg').hide();
			if(assunto=="default"){
				msg_info="<strong>Atenção: </strong>Selecione o motivo de contato.";
				$('.item').css({'background':'#f2dede','border':'1px solid #dd4747'});
				checkInput();
			} else {
				if($('.item[name="nome"]').val()==""){
					msg_info="<strong>Atenção: </strong>Preencha os campos corretamente.";
					$('.item').css({'background':'#f2dede','border':'1px solid #dd4747'});
					checkInput();
				} else if ($('textarea[name="msg"]').val()==""){
					msg_info="<strong>Atenção: </strong>Preencha os campos corretamente.";
					$('.item').css({'background':'#f2dede','border':'1px solid #dd4747'});
					checkInput();
				}
				else {
					$env=true;
				}
			}
		}
		if(msg_info){
			$('#spanmsg').addClass('animated fadeInDown');
			$('#spanmsg').css({'display':'table'});
			$('#spanmsg').addClass('animated fadeIn');
			$('#spanmsg').html(msg_info);
		}

		//resgata dados do form pelo name
		var dados = $('form.formSendContact').serialize();  
		if($env){
	        $.ajax({
	            url: 'inc/sendform.php',
	            dataType: 'html',
	            type: 'POST',
	            data: dados,
	            beforeSend: function() {
	            	$('div.formloading').html(loading); 
	            	$('div.formloading').show();
	            	$('div.formloading').addClass('animated fadeIn');
	            },
	            complete: function() {
	                $(loading).remove(); 
	                $('div.formloading').removeClass('animated fadeIn');
	                $('div.formloading').addClass('animated fadeOut');
	                $('div.formloading').hide();
	            },
	            success: function(data, textStatus) {
	            	if(data!='ok'){
	            		$('div.formspan').show();
	            		$('div.formspan').css({'display':'table'});
	            		$('div.formspan span').addClass('error animated fadeIn');
	            		$('div.formspan span').html('<strong>Falha: </strong>'+data);
	            	} else {
	            		$('div.formspan').show();
	            		$('div.formspan').css({'display':'table'});
	            		$('div.formspan span').addClass('success animated fadeIn');
	            		$('div.formspan span').html('<strong>Sucesso!</strong> Seu contato foi enviado.');
	            		$('form.formSendContact')[0].reset();
	            		$('.item').css({'background':'#fff','border':'none'});
	            	}
	            }
	        });
	    }     
	});
});