//script lightbox
function lightbox() {
	//definindo efeitos
	$('body').css({'overflow':'hidden'});
	$('.lightbox .bdark').addClass('animated fadeIn');
	$('.lightbox .wrapper').addClass('animated fadeIn');
	$('.lightbox a#closelbox').addClass('animated fadeIn');
	//funcoes
	$('.lightbox .bdark').click(function(){
		$('body').css({'overflow-x':'hidden'});
		$('body').css({'overflow-y':'auto'});
		$('.lightbox').fadeOut('slow');
	});
	$('a#closelbox').click(function(e){
		e.preventDefault();
		$('body').css({'overflow-x':'hidden'});
		$('body').css({'overflow-y':'auto'});
		$('.lightbox').fadeOut('slow');
	});
  	document.querySelector('body').addEventListener('keydown', function(event) {
  		var tecla = event.keyCode;
  		if(tecla == 13) {
	  		$('body').css({'overflow-x':'hidden'});
	  		$('body').css({'overflow-y':'auto'});
	  		$('.lightbox').fadeOut('slow');	
  		} else if(tecla == 27) {
	  		$('body').css({'overflow-x':'hidden'});
	  		$('body').css({'overflow-y':'auto'});
	  		$('.lightbox').fadeOut('slow');
  		}
  	});
}